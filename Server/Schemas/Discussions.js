const mongoose = require("mongoose");

// Discussions Schema
const DiscussionsSchema = mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  content: {
    type: String,
    required: true,
  },
  channel: {
    type: String,
    required: true,
    ref: 'Channels'
  },
  nameUser: {
    type: String,
  },
  user: {
    type: mongoose.Schema.ObjectId,
    required: true,
  },
  comments: [
    {
      body: {
        type: String,
        required: true,
      },
      date: {
        type: Date,
        default: Date.now(),
      },
      user: {
        type: mongoose.Types.ObjectId,
        ref: "users",
      },
      userComment: {
        type: String,
        required: true,
      },
    },
  ],
  date: {
    type: Date,
    default: Date.now(),
  },
});

// Generate discussion model
const Discussions = mongoose.model("Discussions", DiscussionsSchema);
module.exports = Discussions;
