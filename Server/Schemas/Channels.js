const mongoose = require("mongoose");

// Schema User
const ChannelsSchema = mongoose.Schema({
  channel: {
    type: String,
    required: true,
  },
});

// Generate Channels Model
const Channels = mongoose.model("Channels", ChannelsSchema);
module.exports = Channels;
