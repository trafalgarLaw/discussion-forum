const mongoose = require('mongoose');
const Schema = mongoose.Schema

// Users Schema
const UsersSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        default: Date.now()
    }
});

// Generate Model Users
const Users = mongoose.model('Users', UsersSchema);
module.exports = Users;
