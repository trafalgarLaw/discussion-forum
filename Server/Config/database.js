const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/DB_discussions', {useNewUrlParser: true,  useUnifiedTopology: true, useFindAndModify: false});

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => {
    // we're connected!
});
