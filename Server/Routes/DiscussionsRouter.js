const KoaRouter = require("koa-router");
const router = new KoaRouter();
const { isAuth } = require("../Middlewares/isAuth");

// Model Users
const Discussions = require("../Schemas/Discussions");

// Read All discussions
router.get("/", async (ctx) => {
  await Discussions.find()
    .then((discussions) => {
      ctx.body = { discussions };
    })
    .catch((err) => {
      console.log(err);
    });
});

// Read a single discussions
router.get("/discussions/:id", async (ctx) => {
  const query = ctx.params.id;
  await Discussions.findOne({ _id: query })
    .then((discussion) => {
      ctx.body = { discussion };
    })
    .catch((err) => {
      console.log(err);
    });
});

// Create a new discussions
router.post("/discussions/create", isAuth, async (ctx) => {
  const { title, content, channel, user, nameUser } = ctx.request.body;

  if (!title || !content || !channel || !user || !nameUser) {
    return (ctx.body = { msg_error: "Please enter all fields" });
  }

  const newDiscussion = await new Discussions({
    title,
    content,
    channel,
    user,
    nameUser,
  });
  await newDiscussion
    .save()
    .then(() => {
      ctx.body = { success: "Discussion saved!", newDiscussion };
    })
    .catch((err) => {
      console.log(err);
    });
});

// Update a discussion
router.put("/discussions/update/:id", isAuth, async (ctx) => {
  const query = ctx.params.id;
  const { title, content, channel } = ctx.request.body;

  if (!title || !content || !channel) {
    return (ctx.body = "Please enter all fields");
  }

  const updateDiscussion = { title, content, channel };

  await Discussions.findOneAndUpdate({ _id: query }, updateDiscussion)
    .then(() => {
      ctx.body = { success: "Discussion Updated!" };
    })
    .catch((err) => {
      console.log(err);
    });
});

// Delete a discussion
router.delete("/discussions/delete/:id", isAuth, async (ctx) => {
  const query = ctx.params.id;

  await Discussions.findOneAndDelete({ _id: query })
    .then((discussion) => {
      if (discussion) {
        ctx.body = { success: "Discussion deleted!", discussion };
      }
    })
    .catch((err) => {
      console.log(err);
    });
});

router.get("/discussions/channel/:name", async (ctx) => {
  const name = ctx.params.name;
  await Discussions.find({ channel: name })
    .then((discussions) => {
      ctx.body = { discussions };
    })
    .catch((err) => {
      console.log(err);
    });
});

// Add Comment
router.post("/comments/:id", isAuth, async (ctx) => {
  const query = ctx.params.id;
  const { body, user, userComment } = ctx.request.body;

  if (!body || !user || !userComment) {
    return (ctx.body = { msg: "Please Enter all fields" });
  }

  await Discussions.findOne({ _id: query })
    .then(async (discussion) => {
      const newComment = { body, user, userComment };

      discussion.comments.unshift(newComment);

      await discussion
        .save()
        .then((discussion) => {
          ctx.body = { success: "Comment saved!", discussion };
        })
        .catch((err) => {
          console.log(err);
        });
    })
    .catch((err) => {
      console.log(err);
    });
});

// edit comment
router.get("/comments/update/:id/:commentId", isAuth, async (ctx) => {
  const discussionsId = ctx.params.id;
  const commentId = ctx.params.commentId;

  await Discussions.findOne({ _id: discussionsId })
    .then((discussion) => {
      if (!discussion) return "discussion does not exist";
      const found = discussion.comments.find(
        (discussion) => discussion._id.toString() === commentId
      );
      ctx.body = { bodyComment: found.body };
    })
    .catch((err) => {
      console.log(err);
    });
});

// Update Comments
router.put("/comments/update/:id/:commentId", isAuth, async (ctx) => {
  const discussionsId = ctx.params.id;
  const commentId = ctx.params.commentId;
  const body = ctx.request.body.body;

  await Discussions.findOne({ _id: discussionsId })
    .then(async (discussion) => {
      if (!discussion) return console.log("discussion does not exist");
      const found = discussion.comments.find(
        (discussion) => discussion._id.toString() === commentId
      );

      found.body = body;
      await discussion
        .save()
        .then(() => {
          ctx.body = { success: "Discussion updated !" };
        })
        .catch((err) => {
          console.log(err);
        });
    })
    .catch((err) => {
      console.log(err);
      ctx.body = err.message;
    });
});

// Delete comments
router.delete("/comments/:id/:commentId", isAuth, async (ctx) => {
  const query = ctx.params.id;
  const queryComment = ctx.params.commentId;

  if (!query || !queryComment) {
    return (ctx.body = { msg: "Please enter all fields" });
  }

  await Discussions.findOne({ _id: query })
    .then(async (discussion) => {
      const filter = discussion.comments.filter(
        (comment) => comment._id.toString() === queryComment
      );

      if (filter.length === 0) {
        return (ctx.body = { msg: "Comment does not exist" });
      }

      const removeComment = discussion.comments
        .map((comment) => comment._id.toString())
        .indexOf(queryComment);
      discussion.comments.splice(removeComment, 1);

      await discussion
        .save()
        .then(() => {
          ctx.body = { removeSuccess: "comments remove success" };
        })
        .catch((err) => {
          console.log(err);
        });
    })
    .catch((err) => {
      ctx.body = { commentNotFound: "No comment found" };
      console.log(err);
    });
});

module.exports = router;
