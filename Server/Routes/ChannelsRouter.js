const KoaRouter = require("koa-router");
const router = new KoaRouter();
const { isAuth } = require("../Middlewares/isAuth");

// Model Channels
const Channels = require("../Schemas/Channels");
const Discussions = require("../Schemas/Discussions");

// All Channels
router.get("/channels", async (ctx) => {
  await Channels.find()
    .then((channels) => {
      ctx.body = { channels };
    })
    .catch((err) => {
      console.log(err);
    });
});

// Create a new Channels
router.post("/channels/create", isAuth, async (ctx) => {
  const { channel } = ctx.request.body;

  if (!channel) return (ctx.body = { error: "Please enter your field" });

  const existChannel = await Channels.findOne({ channel });
  if (existChannel) return (ctx.body = { error: "Channels is already exist" });

  const newChannel = new Channels({ channel });
  await newChannel
    .save()
    .then((channel) => {
      ctx.body = { success: "Create a new Channels", channel };
    })
    .catch((err) => {
      console.log(err);
    });
});

// Update a channel
router.put("/channels/update/:id", isAuth, async (ctx) => {
  const query = ctx.params.id;
  const channel = ctx.request.body;

  if (!channel) return (ctx.body = { msg: "Please Enter all field" });

  await Channels.findOneAndUpdate({ _id: query }, channel)
    .then((channel) => {
      ctx.body = { msg: "channel update" };
    })
    .catch((err) => {
      console.log(err);
    });
});

// Delete Channels
router.delete("/channels/delete/:id", isAuth, async (ctx) => {
  const query = ctx.params.id;

  await Channels.findOneAndDelete({ _id: query })
    .then(async (channel) => {
      await Discussions.deleteMany({ channel: channel.channel })
        .then(() => {
          console.log("Discussions deleted!");
        })
        .catch((err) => {
          console.log(err);
        });
    })
    .catch((err) => {
      console.log(err);
    });
});

module.exports = router;
