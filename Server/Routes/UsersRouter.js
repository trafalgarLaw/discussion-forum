const KoaRouter = require("koa-router");
const router = new KoaRouter();
const argon2 = require("argon2");
const jwt = require("jsonwebtoken");
const { secret } = require("../config/secretKey");
const { isAuth } = require("../Middlewares/isAuth");

// Model Users
const Users = require("../Schemas/Users");

// Sign Up
router.post("/users/signUp", async ctx => {
  const { name, email, password, password2 } = ctx.request.body;

  if (!name || !email || !password || !password2) {
    return (ctx.body = { error: "Please Enter your fields" });
  }

  const existName = await Users.findOne({ name });
  const existEmail = await Users.findOne({ email });

  if (existName) {
    return (ctx.body = { error: "Name is already exist" });
  }

  if (existEmail) {
    return (ctx.body = { error: "Email is already exist" });
  }

  if (password !== password2) {
    return (ctx.body = { error: "Password Do not match" });
  }

  if (password.length < 5) {
    return (ctx.body = { error: "5 characters minimum" });
  }

  const hash = await argon2.hash(password);
  const newUser = new Users({ name, email, password: hash });

  await newUser
    .save()
    .then((user) => {
      ctx.body = { success: "Users Registred!", user };
    })
    .catch((err) => {
      console.log(err);
    });
});

// Sign In
router.post("/users/signIn", async (ctx) => {
  const { name, password } = ctx.request.body;

  if (!name || !password) {
    return (ctx.body = { error: "Please Enter all fields" });
  }

  await Users.findOne({ name })
    .then(async (user) => {
      if (!user) {
        return (ctx.body = { error: "User does not exist" });
      }

      const isMatch = await argon2.verify(user.password, password);
      if (!isMatch) return (ctx.body = { error: "password is not match" });

      const token = jwt.sign({ id: user._id }, secret);
      if (!token) return (ctx.body = { error: "Couldn't sign the token" });

      ctx.body = {
        msg: `Welcome ${user.name}`,
        token,
        user: user._id,
        username: user.name,
      };
    })
    .catch((err) => {
      console.log(err);
    });
});

// Dashboard User
router.get("/users/dashboard/:id", isAuth, async (ctx) => {
  const id = ctx.params.id;

  await Users.findOne({ _id: id })
    .then(async (user) => {
      if (!user) {
        ctx.body = { error: "this is user does not exist" };
        return console.log("this is user does not exist");
      }
      ctx.body = { user };
    })
    .catch((err) => {
      console.log(err);
      ctx.body = err.message;
    });
});

// protected routes
router.get("/protected", isAuth, async (ctx) => {
  ctx.body = "Success Protected";
});

// TODO: create protected routes

module.exports = router;
