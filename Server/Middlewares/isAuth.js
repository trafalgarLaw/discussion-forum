const jwt = require('jsonwebtoken')
const {secret} = require('../Config/secretKey');

module.exports = {
    isAuth: async (ctx, next) => {
      // If Not token
      const token = ctx.headers["jwt_token"];
      if (!token) return (ctx.body = { error: "Token does not exist" });

      // Verify token
      try {
        const verify = jwt.verify(token, secret);
        ctx.request.user = verify.user;
        await next();
      } catch (e) {
        ctx.body = { error: "authorization denied" };
      }
    }
}