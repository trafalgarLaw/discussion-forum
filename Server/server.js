const Koa = require('koa');
const json = require('koa-json');
const bodyparser = require('koa-bodyparser');
const cors = require('@koa/cors');

// Database
require('./Config/database');

// Init server
const server = new Koa();

// koa-Json
server.use(json());

// koa-Body_parser
server.use(bodyparser());

// koa-Cors
server.use(cors());

// Discussions Router
const Discussions = require('./Routes/DiscussionsRouter');
server.use(Discussions.routes()).use(Discussions.allowedMethods());

// Users Router
const Users = require('./Routes/UsersRouter');
server.use(Users.routes()).use(Users.allowedMethods());

// Channels Router
const Channels = require('./Routes/ChannelsRouter');
server.use(Channels.routes()).use(Channels.allowedMethods());

// Server Listen
const PORT = process.env.PORT || 5000;
server.listen(PORT, () => {
    console.log(`Server started run on the port ${PORT}`);
});
