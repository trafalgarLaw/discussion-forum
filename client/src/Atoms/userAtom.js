import { atom } from "recoil";

export const ConnectedState = atom({
  key: "ConnectedState",
  default: false,
});
