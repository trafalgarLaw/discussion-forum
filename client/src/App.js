import React, {useEffect} from 'react';
import Navbar from "./Components/Navbar";
import DiscussionList from "./Components/DiscussionList";
import CreateDiscussion from "./Components/CreateDiscussion";
import ShowDiscussion from "./Components/ShowDiscussion";
import Channels from "./Components/Channels";
import EditDiscussion from "./Components/EditDiscussion";
import Register from "./Components/Register";
import {Switch, Route} from 'react-router-dom';
import Login from "./Components/Login";
import {ConnectedState} from "./Atoms/userAtom";
import {useRecoilState} from "recoil";
import NotFound404 from "./Components/NotFound404";
import DiscussionByChannel from "./Components/DiscussionsByChannel";
import EditComment from "./Components/EditComment";
import DashboardUser from "./Components/DashboardUser";
//import './App.css';

const App = () => {

    const [isConnect, isSetConnect] = useRecoilState(ConnectedState);

    useEffect(() => {
        const fetctSearch = () => {
            if (localStorage['token'] !== undefined && localStorage['user'] !== undefined) {
                isSetConnect(true);
            }
        }
        fetctSearch();
    }, []);

    return (
        <div>
          <Navbar/>
          <div className="container mx-auto w-9/12 text-gray-600 pb-16">
              <Switch>
                  <Route exact path="/" component={DiscussionList}/>
                  <Route path="/discussions/create" component={CreateDiscussion}/>
                  <Route exact path="/discussions/:id" component={ShowDiscussion}/>
                  <Route exact path="/discussions/edit/:id" component={EditDiscussion}/>
                  <Route path="/discussions/channel/:name" component={DiscussionByChannel}/>
                  <Route exact path="/channels" component={Channels}/>
                  <Route exact path="/comments/edit/:id/:commentId" component={EditComment}/>
                  <Route exact path="/users/signUp" component={Register}/>
                  <Route path="/users/signIn" component={Login}/>
                  <Route path="/users/dashboard/:id" component={DashboardUser}/>
                  <Route path="/" component={NotFound404}/>
              </Switch>
          </div>
        </div>
    );
};

export default App;