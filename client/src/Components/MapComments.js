import React from "react";
import { Link } from "react-router-dom";
import { ConnectedState } from "../Atoms/userAtom";
import { useRecoilValue } from "recoil/dist";
import axios from "axios";

const MapComments = ({ id, setComments, comments }) => {
  const isConnect = useRecoilValue(ConnectedState);

  const deleteComment = async (comment_id) => {
    await axios
      .delete(`http://localhost:5000/comments/${id}/${comment_id}`, {
        headers: { jwt_token: localStorage["token"] },
      })
      .then((res) => {
        console.log(res);
        if (res.data.removeSuccess) {
          const filterComment = comments.filter(
            (comment) => comment.user !== comment_id
          );
          setComments(filterComment);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <div>
      {comments
        .map((comment) => (
          <div
            key={comment._id}
            className="border border-gray-300 py-4 px-5 mb-4 rounded"
          >
            <div className="mb-2 flex justify-between">
              <p className="text-blue-300 text-sm inline-flex">
                <span className="text-gray-600 mr-1">Posted by </span>{" "}
                <span>{comment.userComment}</span>
              </p>
              {isConnect && comment.user === localStorage["user"] ? (
                <div className="inline-flex">
                  <Link
                    to={`/comments/edit/${id}/` + comment._id}
                    className="text-blue-500 text-sm mr-2"
                  >
                    Edit
                  </Link>
                  <Link
                    to="#"
                    onClick={() => {
                      deleteComment(comment._id);
                    }}
                    className="text-red-500 text-sm"
                  >
                    Delete
                  </Link>
                </div>
              ) : null}
            </div>
            <p>{comment.body}</p>
          </div>
        ))
        .reverse()}
    </div>
  );
};

export default MapComments;
