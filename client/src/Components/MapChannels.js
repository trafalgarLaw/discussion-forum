import React, { useEffect, useState } from "react";
import axios from "axios";
import { Link } from "react-router-dom";

const MapChannels = () => {
  const [channels, setChannels] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      await axios
        .get("http://localhost:5000/channels", {
          headers: { jwt_token: localStorage["token"] },
        })
        .then((res) => {
          setChannels(res.data.channels);
        })
        .catch((err) => {
          console.log(err);
        });
    };
    fetchData();
  }, [channels]);

  const handleDelete = async (id) => {
    await axios
      .delete(`http://localhost:5000/channels/delete/${id}`, {
        headers: { jwt_token: localStorage["token"] },
      })
      .then((res) => {
        console.log(res);
        const filter = channels.filter((channel) => channel._id !== id);
        setChannels(filter);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <div>
      {channels.map((channel) => (
        <div
          key={channel._id}
          className="flex justify-between w-10/12 mx-auto border border-gray-300 py-5 px-5"
        >
          <h1 className="align-middle">{channel.channel}</h1>
          <div className="align-middle">
            <Link
              to="#"
              className="bg-transparent mr-1 hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded"
            >
              Edit
            </Link>
            <Link
              to="#"
              onClick={() => {
                handleDelete(channel._id);
              }}
              className="bg-red-700 text-white font-bold py-2 px-4 rounded"
            >
              Delete
            </Link>
          </div>
        </div>
      ))}
    </div>
  );
};

export default MapChannels;
