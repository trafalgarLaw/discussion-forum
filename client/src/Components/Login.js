import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { useRecoilState } from "recoil/dist";
import { ConnectedState } from "../Atoms/userAtom";
import axios from "axios";

const Login = () => {
  const [isConnect, isSetConnect] = useRecoilState(ConnectedState);

  const history = useHistory();

  const [name, setName] = useState("");
  const [password, setPassword] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (!name || !password) {
      return console.log("All fields");
    }

    if (password.length < 5) {
      return console.log("5 Characters min");
    }

    const userLogin = { name, password };

    await axios
      .post("http://localhost:5000/users/signIn", userLogin)
      .then((res) => {
        if (res.data.token !== undefined && res.data.user !== undefined) {
          const token = (localStorage["token"] = res.data.token);
          const user = (localStorage["user"] = res.data.user);
          const username = (localStorage["username"] = res.data.username);

          if (
            token !== undefined &&
            user !== undefined &&
            username !== undefined
          ) {
            isSetConnect(true);
            history.push("/");
          }
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <div>
      <h1 className="text-3xl text-black mb-10 text-center font-medium">
        Sign In
      </h1>

      <form action="" onSubmit={handleSubmit}>
        <div className="w-8/12 mx-auto border p-10">
          <div className="flex flex-col mb-4">
            <label
              className="block uppercase text-gray-700 text-xs font-bold mb-2"
              htmlFor="name"
            >
              Name
            </label>
            <input
              onChange={(e) => {
                setName(e.target.value);
              }}
              className="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
              value={name}
              type="text"
              name="name"
              id="name"
            />
          </div>

          <div className="flex flex-col mb-4">
            <label
              className="block uppercase text-gray-700 text-xs font-bold mb-2"
              htmlFor="password"
            >
              Password
            </label>
            <input
              onChange={(e) => {
                setPassword(e.target.value);
              }}
              className="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
              value={password}
              type="password"
              name="password"
              id="password"
            />
          </div>

          <div>
            <button
              className="appearance-none bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 mt-5 rounded"
              type="submit"
            >
              SignIn
            </button>
          </div>
        </div>
      </form>
    </div>
  );
};

export default Login;
