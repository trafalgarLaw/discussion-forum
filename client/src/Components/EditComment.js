import React, {useEffect, useState} from 'react';
import {useHistory} from "react-router-dom";
import axios from 'axios';

const EditComment = (props) => {

    const history = useHistory();

    const id = props.match.params.id;
    const commentId = props.match.params.commentId;

    const [body, setBody] = useState({});

    useEffect(() => {
        const fetchData = async () => {
            await axios.get(`http://localhost:5000/comments/update/${id}/${commentId}`, {headers: {jwt_token: localStorage['token']}})
                .then(res => {
                    setBody(res.data.bodyComment);
                }).catch(err => {
                    console.log(err);
                });
        }
        fetchData()
    }, []);

    const handleSubmit = async (e) => {
        e.preventDefault();
        if (!body) {
            return console.log('please enter field');
        }
        const content = {body};
        await axios.put(`http://localhost:5000/comments/update/${id}/${commentId}`, content, {headers: {jwt_token: localStorage['token']}})
            .then(res => {
                if (res.data.success) {
                    history.push(`/discussions/${id}`);
                }
            }).catch(err => {
                console.log(err);
            });
    };

    return (
        <div>
            <form action="" onSubmit={handleSubmit}>
                <div className="w-11/12 mx-auto border p-10">
                    <div className="flex flex-col mb-4">
                        <label className="block uppercase text-gray-700 text-xs font-bold mb-2" htmlFor="body">body</label>
                        <textarea onChange={(e) => {setBody(e.target.value)}} className="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" value={body} name="body" id="body"/>
                    </div>
                    <div>
                        <button className="appearance-none bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 mt-5 rounded" type="submit">
                            Update comment
                        </button>
                    </div>
                </div>
            </form>
        </div>
    );
};

export default EditComment;
