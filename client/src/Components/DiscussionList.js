import React, {useEffect, useState} from 'react';
import ChannelSidebar from "./ChannelSidebar";
import MapDiscussion from "./MapDiscussion";
import axios from 'axios';

const DiscussionList = () => {

    const [discussions, setDiscussions] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
             axios.get('http://localhost:5000/')
                .then(res => {
                    console.log(res);
                    setDiscussions(res.data.discussions);
                }).catch(err => {
                    console.log(err)
                })
        };
        fetchData();
    }, []);

    return (
        <div className="flex md:flex-row-reverse flex-wrap w-11/12 mx-auto mt-20">
            <div className="w-full md:w-3/4"><MapDiscussion discussions={discussions}/></div>
            <div className="w-full md:w-1/4"><ChannelSidebar/></div>
        </div>
    );
};

export default DiscussionList;
