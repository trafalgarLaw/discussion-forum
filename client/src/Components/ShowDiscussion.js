import React, { Fragment, useEffect, useState } from "react";
import { Link, useHistory } from "react-router-dom";
import { ConnectedState } from "../Atoms/userAtom";
import { useRecoilValue } from "recoil/dist";
import axios from "axios";
import MapComments from "./MapComments";

const ShowDiscussion = (props) => {
  const isConnect = useRecoilValue(ConnectedState);

  const history = useHistory();

  const id = props.match.params.id;

  const [discussion, setDiscussion] = useState({});
  const [comments, setComments] = useState([]);
  const [body, setBody] = useState("");

  const countComments = comments.length;

  useEffect(() => {
    const fetchData = () => {
      axios
        .get(`http://localhost:5000/discussions/${id}`)
        .then((res) => {
          setDiscussion(res.data.discussion);
          setComments(res.data.discussion.comments);
        })
        .catch((err) => {
          console.log(err);
        });
    };
    fetchData();
  }, [comments]);

  const handleEdit = (id) => {
    history.push("/discussions/edit/" + id);
  };

  const handleDelete = async (id) => {
    await axios
      .delete(`http://localhost:5000/discussions/delete/${id}`, {
        headers: { jwt_token: localStorage["token"] },
      })
      .then((res) => {
        if (res.data.success) history.push("/");
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (!body) return console.log("Please enter the field");

    const user = localStorage["user"];
    const userComment = localStorage["username"];

    const newComment = { body, user, userComment };

    await axios
      .post(`http://localhost:5000/comments/${id}`, newComment, {
        headers: { jwt_token: localStorage["token"] },
      })
      .then((res) => {
        if (res.data.success) {
          history.push(`/discussions/${id}`);
          setBody("");
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <div className="w-10/12 mx-auto">
      {isConnect && discussion.user === localStorage["user"] ? (
        <Fragment>
          <div className="mb-4">
            <button
              onClick={() => {
                handleEdit(discussion._id);
              }}
              className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded mr-1"
            >
              Edit
            </button>

            <button
              onClick={() => {
                handleDelete(discussion._id);
              }}
              className="bg-red-600 text-white font-bold py-2 px-4 rounded"
            >
              Delete
            </button>
          </div>
        </Fragment>
      ) : null}

      <div className="px-8 py-2 mb-5 border border-gray-300 rounded">
        <div className="align-middle">
          <h1 className="inline-block align-middle text-4xl text-black">
            {discussion.title}
          </h1>
          <span className="float-right mt-4 align-middle">
            <Link
              className="hover:text-blue-500"
              to={"/discussions/channel/" + discussion.channel}
            >
              {discussion.channel}
            </Link>
          </span>
        </div>
        <p className="inline-block mb-6 text-blue-500 text-xl">
          By <span className="font-medium">{discussion.nameUser}</span>
        </p>
        <p className="pb-0 mb-8">{discussion.content}</p>
      </div>

      {comments.length !== 0 ? (
        <h1 className="text-2xl mb-8 text-black">{countComments} Comments</h1>
      ) : null}

      <MapComments comments={comments} setComments={setComments} id={id} />

      {isConnect ? (
        <Fragment>
          <h1 className="text-xl mb-3 mt-8 text-black">Reply</h1>

          <form onSubmit={handleSubmit}>
            <div className="flex flex-col mb-1">
              <textarea
                onChange={(e) => {
                  setBody(e.target.value);
                }}
                className="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
                rows="3"
                name="body"
                value={body}
                id="content"
              />
            </div>

            <button
              className="appearance-none bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 mt-1 rounded"
              type="submit"
            >
              Add a comment
            </button>
          </form>
        </Fragment>
      ) : null}
    </div>
  );
};

export default ShowDiscussion;
