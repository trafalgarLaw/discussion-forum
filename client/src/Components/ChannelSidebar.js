import React, {Fragment, useEffect, useState} from 'react';
import axios from 'axios';
import {useHistory, Link} from 'react-router-dom';

const ChannelSidebar = () => {

    const history = useHistory();

    const [channels, setChannels] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            await axios.get('http://localhost:5000/channels')
                .then(res => {
                    setChannels(res.data.channels)
                }).catch(err => {
                    console.log(err);
                })
        }
        fetchData();
    }, []);

    const handleClick = () => {
        history.push('/discussions/create');
    };

    return (
        <div className="">
            <button onClick={handleClick}
                className="bg-blue-500 mb-3 hover:bg-blue-500 text-white font-semibold py-2 px-6 border border-blue-500 rounded">
                New Discussion
            </button>
            <ul>
                <li className="mb-2 text-base hover:text-blue-400 ml-10"><Link to="/">All threads</Link></li>
            </ul>
            {
                channels.map(channel =>
                   <Fragment>
                       <ul key={channel._id} className="list-none">
                           <li className="mb-2 text-base hover:text-blue-400 ml-10"><Link to={"/discussions/channel/" + channel.channel}>{channel.channel}</Link></li>
                       </ul>
                   </Fragment>
                )
            }
        </div>
    );
};

export default ChannelSidebar;