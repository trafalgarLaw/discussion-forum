import React from 'react';
import ChannelSidebar from "./ChannelSidebar";
import MapDiscussionsByChannel from "./MapDiscussionsByChannel";

const DiscussionByChannel = (props) => {

    const name = props.match.params.name;

    return (
        <div className="flex md:flex-row-reverse flex-wrap w-11/12 mx-auto mt-20">
            <div className="w-full md:w-3/4"><MapDiscussionsByChannel name={name}/></div>
            <div className="w-full md:w-1/4"><ChannelSidebar/></div>
        </div>
    );
};

export default DiscussionByChannel;
