import React, { Fragment } from "react";
import { Link, useHistory } from "react-router-dom";
import { ConnectedState } from "../Atoms/userAtom";
import { useRecoilState } from "recoil";

const Navbar = () => {
  const history = useHistory();

  const [isConnect, isSetConnect] = useRecoilState(ConnectedState);

  const handleLogout = (e) => {
    e.preventDefault();
    localStorage.clear();
    isSetConnect(false);
    history.push("/users/signIn");
  };

  return (
    <div>
      <nav className="flex items-center mb-10 justify-between flex-wrap bg-teal-500 p-6">
        <div className="flex items-center flex-shrink-0 text-white mr-6">
          <svg
            className="fill-current h-8 w-8 mr-2"
            width="54"
            height="54"
            viewBox="0 0 54 54"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path d="M13.5 22.1c1.8-7.2 6.3-10.8 13.5-10.8 10.8 0 12.15 8.1 17.55 9.45 3.6.9 6.75-.45 9.45-4.05-1.8 7.2-6.3 10.8-13.5 10.8-10.8 0-12.15-8.1-17.55-9.45-3.6-.9-6.75.45-9.45 4.05zM0 38.3c1.8-7.2 6.3-10.8 13.5-10.8 10.8 0 12.15 8.1 17.55 9.45 3.6.9 6.75-.45 9.45-4.05-1.8 7.2-6.3 10.8-13.5 10.8-10.8 0-12.15-8.1-17.55-9.45-3.6-.9-6.75.45-9.45 4.05z" />
          </svg>
          <span className="font-semibold text-xl tracking-tight">
            <Link to="/">Discussion</Link>
          </span>
        </div>
        <div className="block lg:hidden">
          <button className="flex items-center px-3 py-2 border rounded text-teal-200 border-teal-400 hover:text-white hover:border-white">
            <svg
              className="fill-current h-3 w-3"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <title>Menu</title>
              <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z" />
            </svg>
          </button>
        </div>
        <div className="w-full block flex-grow lg:flex lg:items-center lg:w-auto">
          <div className="text-sm lg:flex-grow">
            <Link
              to="/"
              className="block mt-4 lg:inline-block lg:mt-0 text-teal-200 hover:text-white mr-4"
            >
              Discussion
            </Link>
            {isConnect ? (
              <Fragment>
                <Link
                  to="/channels"
                  className="block mt-4 lg:inline-block lg:mt-0 text-teal-200 hover:text-white mr-4"
                >
                  Channels
                </Link>
              </Fragment>
            ) : null}
          </div>
          <div>
            {isConnect ? (
              <Fragment>
                <Link
                  to={"/users/dashboard/" + localStorage["user"]}
                  className="block mt-4 lg:inline-block lg:mt-0 text-teal-200 hover:text-white mr-4"
                >
                  {localStorage["username"]}
                </Link>
                <Link
                  to="#responsive-header"
                  onClick={handleLogout}
                  className="block mt-4 lg:inline-block lg:mt-0 text-teal-200 hover:text-white mr-4"
                >
                  Logout
                </Link>
              </Fragment>
            ) : (
              <Fragment>
                <Link
                  to="/users/signIn"
                  className="block mt-4 lg:inline-block lg:mt-0 text-teal-200 hover:text-white mr-4"
                >
                  SignIn
                </Link>
                <Link
                  to="/users/signUp"
                  className="block mt-4 lg:inline-block lg:mt-0 text-teal-200 hover:text-white mr-4"
                >
                  SignUp
                </Link>
              </Fragment>
            )}
          </div>
        </div>
      </nav>
    </div>
  );
};

export default Navbar;
