import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import moment from "moment";
import axios from "axios";

const MapDiscussionsByChannel = ({ name }) => {
  const [discussions, setDiscussions] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      await axios
        .get(`http://localhost:5000/discussions/channel/${name}`)
        .then((res) => {
          if (res.data.discussions) {
            setDiscussions(res.data.discussions);
          }
        })
        .catch((err) => {
          console.log(err);
        });
    };
    fetchData();
  }, [discussions]);

  return (
    <div>
      {discussions.length === 0 ? (
        <p className="text-gray-500 font-bold text-5xl text-center">No post</p>
      ) : (
        discussions
          .map((discussion) => (
            <Link to={"/discussions/" + discussion._id}>
              <div key={discussion._id} className="border mb-3 rounded p-4">
                <div className="align-middle mb-2 ">
                  <h3 className="inline-block text-xl font-medium text-gray-700 py-0 ">
                    <Link to={"/discussions/" + discussion._id}>
                      {discussion.title}
                    </Link>
                  </h3>
                  <p className="inline-block float-right text-gray-700 font-light text-sm mb-5">
                    <span className="font-medium" to="#">
                      {discussion.channel.toString().toUpperCase()}
                    </span>
                  </p>
                </div>
                <p className="italic text-blue-500 text-sm">
                  Posted by{" "}
                  <span className="font-medium">{discussion.nameUser}, </span>
                  <span className="text-gray-600">
                    {moment(discussion.date).format("ll")}
                  </span>
                </p>
              </div>
            </Link>
          ))
          .reverse()
      )}
    </div>
  );
};

export default MapDiscussionsByChannel;
