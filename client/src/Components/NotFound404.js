import React from "react";

const NotFound404 = () => {
  return (
    <div>
      <h1 className="text-6xl text-black">Error 404</h1>
    </div>
  );
};

export default NotFound404;
