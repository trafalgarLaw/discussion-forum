import React, {useEffect, useState} from 'react';
import axios from 'axios';

const DashboardUser = (props) => {

    const id = props.match.params.id;

    const [user, setUser] = useState({});

    useEffect(() => {
        const fetchUser = async () => {
            await axios.get(`http://localhost:5000/users/dashboard/${id}`, {headers: {jwt_token: localStorage['token']}})
                .then(res => {
                    console.log(res);
                    setUser(res.data.user);
                }).catch(err => {
                    console.log(err);
                })
        };
        fetchUser();
    }, []);

    return (
        <div>
            <h1 className="text-3xl text-black mb-6 text-center font-medium">Dashboard User</h1>

            <p className="text-2xl text-black font-medium mb-2">Name: {user.name}</p>
            <p className="text-2xl text-black font-medium mb-6">Email: {user.email}</p>

            <button className="bg-blue-500 mb-3 hover:bg-blue-500 text-white font-semibold py-2 px-6 border border-blue-500 rounded">Edit Profile</button>
        </div>
    );
};

export default DashboardUser;
