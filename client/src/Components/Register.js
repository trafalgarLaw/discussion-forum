import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";

const Register = () => {
  const history = useHistory();

  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [password2, setPassword2] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (!name || !email || !password || !password2) {
      return console.log("All fields");
    }

    if (password.length < 5) {
      return console.log("5 Characters min");
    }

    if (password !== password2) {
      return console.log("Password is not match");
    }

    const newUser = { name, email, password, password2 };
    console.log(newUser);

    await axios
      .post("http://localhost:5000/users/signUp", newUser)
      .then((res) => {
        console.log(res);
        if (res.data.success) history.push("/users/signIn");
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <div>
      <h1 className="text-3xl text-black mb-10 text-center font-medium">
        Sign Up
      </h1>

      <form action="" onSubmit={handleSubmit}>
        <div className="w-8/12 mx-auto border p-10">
          <div className="flex flex-col mb-4">
            <label
              className="block uppercase text-gray-700 text-xs font-bold mb-2"
              htmlFor="name"
            >
              Name
            </label>
            <input
              onChange={(e) => {
                setName(e.target.value);
              }}
              className="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
              value={name}
              type="text"
              name="name"
              id="name"
            />
          </div>

          <div className="flex flex-col mb-4">
            <label
              className="block uppercase text-gray-700 text-xs font-bold mb-2"
              htmlFor="email"
            >
              Email
            </label>
            <input
              onChange={(e) => {
                setEmail(e.target.value);
              }}
              className="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
              value={email}
              type="email"
              name="email"
              id="email"
            />
          </div>

          <div className="flex flex-col mb-4">
            <label
              className="block uppercase text-gray-700 text-xs font-bold mb-2"
              htmlFor="password"
            >
              Password
            </label>
            <input
              onChange={(e) => {
                setPassword(e.target.value);
              }}
              className="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
              value={password}
              type="password"
              name="password"
              id="password"
            />
          </div>

          <div className="flex flex-col mb-4">
            <label
              className="block uppercase text-gray-700 text-xs font-bold mb-2"
              htmlFor="password2"
            >
              Confirm password
            </label>
            <input
              onChange={(e) => {
                setPassword2(e.target.value);
              }}
              className="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
              value={password2}
              type="password"
              name="password2"
              id="password2"
            />
          </div>

          <div>
            <button
              className="appearance-none bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 mt-5 rounded"
              type="submit"
            >
              SignUp
            </button>
          </div>
        </div>
      </form>
    </div>
  );
};

export default Register;
