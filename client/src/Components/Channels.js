import React, {useState} from 'react';
//import {useHistory} from 'react-router-dom';
import MapChannels from "./MapChannels";
import axios from 'axios';

const Channels = () => {

    //const history = useHistory();

    const [channel, setChannel] = useState("");

    const handleSubmit = async (e) => {
        e.preventDefault();

        if (!channel) return console.log("Please enter the field");
        const newChannel = {channel};

        await axios.post('http://localhost:5000/channels/create', newChannel, {headers: {jwt_token: localStorage['token']}})
            .then(res => {
                if (res.data.success) {
                    setChannel('');
                }
            }).catch(err => {
                console.log(err);
            })
    }

    return (
        <div className="w-10/12 mx-auto">
            <h1 className="text-3xl text-black mb-6 text-center font-medium">Channels</h1>

            <form action="" onSubmit={handleSubmit}>
                <div className="flex flex-col mb-16 w-10/12 mx-auto">
                    <label className="block uppercase text-gray-700 text-xs font-bold mb-2" htmlFor="channel">Channel</label>
                    <input type="text" value={channel} onChange={(e) => {setChannel(e.target.value)}} placeholder="Channel" className="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" id="channel"/>
                    <button type="submit" className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">Create channel</button>
                </div>
            </form>

            <MapChannels/>

        </div>
    );
};

export default Channels;
