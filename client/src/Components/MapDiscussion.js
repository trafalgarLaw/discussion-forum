import React from "react";
import { Link } from "react-router-dom";
import moment from "moment";
import { BsFillChatFill } from "react-icons/bs";

const MapDiscussion = ({ discussions }) => {
  return (
    <div className="">
      {discussions.length === 0 ? (
        <p className="text-gray-500 font-bold text-5xl text-center">No post</p>
      ) : (
        discussions
          .map((discussion) => (
            <Link to={"/discussions/" + discussion._id}>
              <div key={discussion._id} className="border mb-3 rounded p-4">
                <div className="align-middle mb-2 ">
                  <h3 className="inline-block text-xl font-medium text-gray-700 py-0 ">
                    <Link to={"/discussions/" + discussion._id}>
                      {discussion.title}
                    </Link>
                  </h3>
                  <p className="inline-block float-right text-gray-700 font-medium text-sm mb-5">
                    <Link
                      className="hover:text-blue-500"
                      to={"/discussions/channel/" + discussion.channel}
                    >
                      {discussion.channel.toString().toUpperCase()}
                    </Link>
                  </p>
                  {discussion.comments.length > 0 ? (
                    <p className="inline-block float-right text-gray-700 font-light text-sm mb-5 mr-6">
                      {" "}
                      <BsFillChatFill
                        color="gray"
                        className="inline mr-1"
                      />{" "}
                      <span className="font-medium">
                        {discussion.comments.length}
                      </span>
                    </p>
                  ) : null}
                </div>
                <p className="text-blue-500 text-sm">
                  Posted by{" "}
                  <span className="font-medium">{discussion.nameUser}, </span>
                  <span className="text-gray-600">
                    {moment(discussion.date).format("ll")}
                  </span>
                </p>
              </div>
            </Link>
          ))
          .reverse()
      )}
    </div>
  );
};

export default MapDiscussion;
