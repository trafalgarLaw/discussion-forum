import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";

const EditDiscussion = (props) => {
  const history = useHistory();

  const id = props.match.params.id;

  const [title, setTitle] = useState("");
  const [content, setContent] = useState("");
  const [channel, setChannel] = useState("");

  const [mapChannel, setMapChannel] = useState([]);

  useEffect(() => {
    const fetchDataDiscuccion = async () => {
      await axios
        .get(`http://localhost:5000/discussions/${id}`)
        .then((res) => {
          setTitle(res.data.discussion.title);
          setContent(res.data.discussion.content);
          setChannel(res.data.discussion.channel);
        })
        .catch((err) => {
          console.log(err);
        });
    };

    const fetchDataAllChannels = async () => {
      await axios
        .get("http://localhost:5000/channels", {
          headers: { jwt_token: localStorage["token"] },
        })
        .then((res) => {
          setMapChannel(res.data.channels);
        })
        .catch((err) => {
          console.log(err);
        });
    };

    fetchDataDiscuccion();
    fetchDataAllChannels();
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (!title || !content || !channel)
      return console.log("Please enter all fields");
    const updateDiscussion = { title, content, channel };

    await axios
      .put(`http://localhost:5000/discussions/update/${id}`, updateDiscussion, {
        headers: { jwt_token: localStorage["token"] },
      })
      .then((res) => {
        if (res.data.success) history.push("/discussions/" + id);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <div className="w-10/12 mx-auto">
      <h1 className="text-3xl text-black mb-10 text-center font-medium">
        Update Discussion
      </h1>

      <form onSubmit={handleSubmit}>
        <div className="w-11/12 mx-auto border p-10">
          <div className="flex flex-col mb-4">
            <label
              className="block uppercase text-gray-700 text-xs font-bold mb-2"
              htmlFor="title"
            >
              Title
            </label>
            <input
              onChange={(e) => {
                setTitle(e.target.value);
              }}
              className="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
              value={title}
              type="text"
              name="title"
              id="title"
            />
          </div>

          <div className="flex flex-col mb-4">
            <label
              className="block border-gray-200 leading-tight uppercase text-gray-700 text-xs font-bold mb-2"
              htmlFor="content"
            >
              Content
            </label>
            <textarea
              onChange={(e) => {
                setContent(e.target.value);
              }}
              className="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
              rows="5"
              name="content"
              value={content}
              id="content"
            />
          </div>

          <div className="w-full md:w-1/3 mb-6 md:mb-0">
            <label
              className="block uppercase text-gray-700 text-xs font-bold mb-2"
              htmlFor="channel"
            >
              Channel
            </label>
            <div className="relative">
              <select
                onChange={(e) => {
                  setChannel(e.target.value);
                }}
                value={channel}
                name="channel"
                className="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                id="channel"
              >
                <option value="" selected>
                  --Please choose a channel--
                </option>
                {mapChannel.map((channel) => (
                  <option key={channel._id} defaultValue={channel.channel}>
                    {channel.channel}
                  </option>
                ))}
              </select>
              <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                <svg
                  className="fill-current h-4 w-4"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 20 20"
                >
                  <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                </svg>
              </div>
            </div>
          </div>
          <div>
            <button
              className="appearance-none bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 mt-5 rounded"
              type="submit"
            >
              Update discussion
            </button>
          </div>
        </div>
      </form>
    </div>
  );
};

export default EditDiscussion;
